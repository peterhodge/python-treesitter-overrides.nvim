; extends

; Attempt 1
; this doesn't work because it also matches "import something.json"
;(import_statement
;  (dotted_name
;    ((identifier) @foo (#eq? @foo "json")) @import.known.json
;  )
;)

; Attempt 2
; this doesn't seem to work because the (#eq?...) predicate doesn't get the
; text contents of the (dotted_name) capture for some reason
;(import_statement
;  ((dotted_name) @foo (#eq? @foo "os")) @import.known.json
;)

; # Attempt 3
; ????
;(
;  (import_statement
;    dn: (dotted_name) @_dotname
;  )
;  (#any-of? @_dotname "os" "glob")
;)

; # Attempt 4
; this works - names and dotted-names below are highlighted correctly in import statements
(
  (import_statement (dotted_name) @import.known.stdlib @_dotname)
  (#any-of? @_dotname
    ; TODO: generate list of builtin modules for python 3.X
    "platform" "glob" "os" "os.path"
  )
  ; setting priority seems to be necessary when we are extending the existing highlights queries
  (#set! "priority" 110)
)

; TODO:
; - I want to create a custom directive for import statements to store imported names in a table to highlight below
; - I then want to create a custom predicate to match the imported names when they appear in the code below



; this works a little better at matching stdlib stuff, but only the first module name is highlighted
;(import_statement
;  (dotted_name
;    .
;    ((identifier) @foo (#eq? @foo "os")) @import.known.stdlib
;  )
;)

; this works a little better at matching stdlib stuff, but only the first module name is highlighted
;(import_statement
;  (dotted_name
;    .
;    ((identifier) @foo (#eq? @foo "os"))
;  )
;  @import.known.stdlib
;)

;(import_statement
;  (dotted_name
;    ((identifier) @foo (#eq? @foo "os")) @import.known.stdlib
;  )
;)

;(import_statement
;  ((dotted_name) @foo (#match? @foo "^glob$")) @import.known.stdlib
;  (#set! "priority" 110)
;asdfasdfasd)
;(import_statement
;  (dotted_name
;    ((identifier) @foo (#any-of? @foo "re" "os"))
;  )
;  @import.known.stdlib
;  (#set! "priority" 110)
;)

; XXX: this query doesn't quite work in that "something.glob" is also matched even though we have an anchor there
;(import_statement
;  (dotted_name
;    .
;    ((identifier) @foo (#any-of? @foo "platform" "glob"))
;  )
;  @import.known.stdlib
;  (#set! "priority" 110)
;)

; this doesn't work at all sorry
;(import_statement
;  (dotted_name
;    .
;    [
;      ((identifier) @foo (#eq? @foo "platform"))
;      ((identifier) @foo (#eq? @foo "glob"))
;    ]
;  )
;  @import.known.stdlib
;  (#set! "priority" 110)
;)


; fix "None" highlighting
;(
;  (none)
;  @type
;  (#set! "priority" 110)
;)
(none) @type

(list (["[" "]" ","] @type))
(subscript ((slice) (["[" "]" ":"] @type)))
;(slice (["[" "]" ":"] @type))
